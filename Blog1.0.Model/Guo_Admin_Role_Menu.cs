﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Blog1._0.Model
{
    /// <summary>
    /// 菜单权限
    /// </summary>
    public partial class Guo_Admin_Role_Menu
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int RowId { get; set; }
        /// <summary>
        /// 菜单ID
        /// </summary>
        public int? MenuId { get; set; }
        /// <summary>
        /// 角色ID
        /// </summary>
        public int RoleId { get; set; }
        /// <summary>
        /// 操作ID
        /// </summary>
        public int ActionId { get; set; }
    }
}
