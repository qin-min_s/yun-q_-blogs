﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Blog1._0.Model
{
    public partial class Guo_Sys_Menu
    {
        public short MenuID { get; set; }
        public Nullable<short> LFT { get; set; }
        public Nullable<short> RGT { get; set; }
        public Nullable<short> LVL { get; set; }
        public short REL { get; set; }
        public string Caption { get; set; }
        public string URL { get; set; }
        public Nullable<bool> IsSystem { get; set; }
        public Nullable<bool> IsHid { get; set; }
        public Nullable<short> BlockID { get; set; }
        public int Par_ID { get; set; }
        public string icon { get; set; }
    }
}
