﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Blog1._0.Model
{
    public partial class Guo_SysLog
    {
        /// <summary>
        /// 主键ID
        /// </summary>
        public int RowId { get; set; }
        /// <summary>
        /// 日志类型
        /// </summary>
        public string Type { get; set; }
        /// <summary>
        /// 记录时间
        /// </summary>
        public DateTime Time { get; set; }
        /// <summary>
        /// 操作员ID
        /// </summary>
        public int OP_Eid { get; set; }
        /// <summary>
        /// 操作员名称
        /// </summary>
        public string OP_Ename { get; set; }
        /// <summary>
        /// ip
        /// </summary>
        public string Ip { get; set; }
        /// <summary>
        /// ip主机名
        /// </summary>
        public string IpAddressName { get; set; }
        /// <summary>
        /// 描述
        /// </summary>
        public string Describe { get; set; }
        /// <summary>
        /// 操作结果
        /// </summary>
        public bool Results { get; set; }
    }
}
