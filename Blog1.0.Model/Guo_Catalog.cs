﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Blog1._0.Model
{
    public partial class Guo_Catalog
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int CatalogID { get; set; }
        /// <summary>
        /// 分类名称
        /// </summary>
        public string CatalogName { get; set; }
        /// <summary>
        /// 创建者ID
        /// </summary>
        public int UserID { get; set; }
        /// <summary>
        /// 创建者
        /// </summary>
        public string UserName { get; set; }
        /// <summary>
        /// 排序
        /// </summary>
        public Nullable<int> OrderNo { get; set; }
        /// <summary>
        /// 是否启用
        /// </summary>
        public Nullable<bool> IsActive { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        public Nullable<System.DateTime> CRT_Time { get; set; }
    }
}
