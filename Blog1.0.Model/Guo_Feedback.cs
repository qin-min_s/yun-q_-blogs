﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Blog1._0.Model
{
    public partial class Guo_Feedback
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]//代表ID自增
        public int Id { get; set; }
        /// <summary>
        /// 人员ID
        /// </summary>
        public int SendId { get; set; }
        /// <summary>
        /// 目标人员ID
        /// </summary>
        public int AcceptId { get; set; }
        /// <summary>
        /// 内容
        /// </summary>
        public string Content { get; set; }
        /// <summary>
        /// 父ID
        /// </summary>
        public int ParentId { get; set; }
        /// <summary>
        /// 城市
        /// </summary>
        public string City { get; set; }
        /// <summary>
        /// 登录设备
        /// </summary>
        public string Equip { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        public System.DateTime CreateOn { get; set; }
    }
}
