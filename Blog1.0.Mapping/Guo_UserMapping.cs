﻿using Blog1._0.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Blog1._0.Mapping
{
    public class Guo_UserMapping: EntityTypeConfiguration<Guo_User>
    {
        public Guo_UserMapping()
        {
            this.ToTable("Guo_User");
            this.HasKey(t => t.ID);
        }
    }
}
