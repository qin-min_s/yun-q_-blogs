﻿using Blog1._0.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Blog1._0.Mapping
{
    public class Guo_RoleMapping : EntityTypeConfiguration<Guo_Role>
    {
        public Guo_RoleMapping()
        {
            this.ToTable("Guo_Role");
            this.HasKey(t => t.Id);
        }
    }
}
