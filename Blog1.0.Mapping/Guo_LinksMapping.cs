﻿using Blog1._0.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Blog1._0.Mapping
{
    public class Guo_LinksMapping : EntityTypeConfiguration<Guo_Links>
    {
        public Guo_LinksMapping()
        {
            this.ToTable("Guo_Links");
            this.HasKey(t => t.Id);
        }
    }
}
