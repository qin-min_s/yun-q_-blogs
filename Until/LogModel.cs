﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Until
{
    public class LogModel
    {
        public int Id { get; set; }
        /// <summary>
		/// LogType
        /// </summary>
        public string LogType { get; set; }
        /// <summary>
        /// Account
        /// </summary>
        public string RealName { get; set; }
        /// <summary>
        /// 用户名称
        /// </summary>
        public string UserName { get; set; }
        /// <summary>
        /// Description
        /// </summary>
        public string Description { get; set; }
        /// <summary>
        /// ModuleName
        /// </summary>
        public string ModuleName { get; set; }
        /// <summary>
        /// IPAddress
        /// </summary>
        public string IPAddress { get; set; }
        /// <summary>
        /// IPAddressName
        /// </summary>
        public string IPAddressName { get; set; }
        public DateTime UpdateOn { get; set; }
        public int UpdateBy { get; set; }
        public int CreateBy { get; set; }
    }
}
