﻿namespace Until
{
    /// <summary>
    /// 分页 类
    /// </summary>
    public class PageInfo
    {
        public int page { get; set; }
        public int limit { get; set; }
        /// <summary>
        /// 排序字段 CreateOn
        /// </summary>
        public string field { get; set; }
        /// <summary>
        /// 排序方式 asc desc
        /// </summary>
        public string order { get; set; }
        /// <summary>
        /// 返回字段逗号分隔
        /// </summary>
        public string returnFields { get; set; }
        public string prefix { get; set; }
    }
}
