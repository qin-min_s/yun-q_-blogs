﻿using Blog1._0.Data;
using Blog1._0.Model;
using Until;

namespace Blog1._0.Service
{
    /// <summary>
    /// 【修改密码】逻辑层
    /// </summary>
    public class Admin_PassWordService : Repository
    {
        /// <summary>
        /// 验证原密码是否正确
        /// </summary>
        /// <param name="Pwd"></param>
        /// <returns></returns>
        public bool CheckPwd(string Pwd)
        {
            var db = this.Base();
            var Ids = Current.GetEntity().OperatorId;
            var user = db.FindEntity<Guo_User>(t => t.ID == Ids);
            if (user.PassWord == Pwd)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 修改密码
        /// </summary>
        /// <param name="Pwd"></param>
        /// <returns></returns>
        public bool ModifyPwd(PassWordModel model)
        {
            var db = this.Base();
            var Ids = Current.GetEntity().OperatorId;
            var user = db.FindEntity<Guo_User>(t => t.ID == Ids);
            user.PassWord = Md5s.md5(model.Password,32);

            db.Update<Guo_User>(user);
            return db.Commit() > 0 ? true : false;
        }
    }
}
