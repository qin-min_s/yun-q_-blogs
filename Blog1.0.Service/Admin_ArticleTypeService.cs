﻿using Blog1._0.Data;
using Blog1._0.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Until;

namespace Blog1._0.Service
{
    /// <summary>
    /// 【文章类型】逻辑层
    /// </summary>
    public class Admin_ArticleTypeService : Repository
    {
        /// <summary>
        /// 数据加载
        /// </summary>
        /// <param name="filter"></param>
        /// <param name="pageInfo"></param>
        /// <returns></returns>
        public LayuiResult GetListByFilter(Guo_Type filter, PageInfo pageInfo)
        {
            var db = this.Base();
            var diary = db.IQueryable<Guo_Type>();
            var query = (from i in diary
                         orderby i.CRT_Time descending
                         select i).Skip((pageInfo.page - 1) * pageInfo.limit).Take(pageInfo.limit);

            return new LayuiResult { code = 0, count = diary.Count(), data = query.ToList() };
        }

        /// <summary>
        /// 新增
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public bool CreateModel(ArticleTypeModel model)
        {
            var db = this.Base();
            Guo_Type diay = new Guo_Type();
            db.SaveChanges();
            diay.TypeName = model.TypeName;
            diay.UserID = Current.GetEntity().OperatorId;
            diay.UserName = Current.GetEntity().UserName;
            diay.CRT_Time = model.CRT_Time;

            db.Insert<Guo_Type>(diay);
            //提交失败，则检查实体类是否加上自增ID标识
            return db.Commit() > 0 ? true : false;
        }

        /// <summary>
        /// 详情
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public IEnumerable<ArticleTypeModel> ReadModel(int Id)
        {
            var db = this.Base();
            var data = db.IQueryable<Guo_Type>().Where(t => t.TypeID == Id);
            var user = db.IQueryable<Guo_User>();
            var query = from i in data
                        join t in user
                        on i.UserID equals t.ID
                        select new ArticleTypeModel()
                        {
                            TypeID = i.TypeID,
                            TypeName = i.TypeName,
                            UserName = t.UserName,
                            CRT_Time = i.CRT_Time
                        };

            return query.ToList();
        }

        /// <summary>
        /// 修改
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public bool UpdateList(ArticleTypeModel model)
        {
            var db = this.Base();
            var res = db.IQueryable<Guo_Type>(t => t.TypeID == model.TypeID).FirstOrDefault();
            res.TypeName = model.TypeName;

            db.Update(res);
            return db.Commit() > 0 ? true : false;
        }

        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public bool DeleteModel(int Id)
        {
            var db = this.Base();
            try
            {
                var entity = db.FindEntity<Guo_Type>(t => t.TypeID == Id);
                if (entity == null)
                {
                    return false;
                }
                db.Delete<Guo_Type>(t => t.TypeID == entity.TypeID);
                return db.Commit() > 0 ? true : false;
            }
            catch (Exception)
            {
                throw new Exception("删除失败！");
            }
        }
    }
}
