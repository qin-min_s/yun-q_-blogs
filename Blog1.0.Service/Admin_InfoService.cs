﻿using Blog1._0.Data;
using Blog1._0.Model;
using System.Collections.Generic;
using System.Linq;
using Until;

namespace Blog1._0.Service
{
    /// <summary>
    /// 【基本资料】逻辑层
    /// </summary>
    public class Admin_InfoService : Repository
    {
        /// <summary>
        /// 基本资料查询
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public IEnumerable<UserModel> GetRoleList(int Id)
        {
            var db = this.Base();
            var user = db.FindList<Guo_User>(t => t.ID == Id);
            var roles = db.FindList<Guo_Role>();
            var query = from i in user
                        join r in roles
                        on i.RoleId equals r.Id
                        select new UserModel()
                        {
                            Id = i.ID,
                            UserName = i.LoginID,
                            RealName = i.UserName,
                            Gender = i.Gender,
                            HeadShot = i.HeadShot,
                            Phone = i.Phone,
                            Email = i.Email,
                            Remark = i.Remark,
                            RoleName = r.UserName
                        };

            return query;
        }

        /// <summary>
        /// 修改
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public bool UpdateList(UserModel model)
        {
            var db = this.Base();
            var res = db.IQueryable<Guo_User>(t => t.ID == model.Id).FirstOrDefault();
            res.UserName = model.RealName;
            res.Gender = model.Gender;
            res.HeadShot = model.HeadShot;
            res.Phone = model.Phone;
            res.Email = model.Email;
            res.Remark = model.Remark;
            res.UpdateOn = model.UpdateOn;
            res.UpdateBy = model.UpdateBy;

            db.Update(res);
            return db.Commit() > 0 ? true : false;
        }
    }
}
