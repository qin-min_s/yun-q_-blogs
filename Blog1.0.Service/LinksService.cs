﻿using Blog1._0.Data;
using Blog1._0.Model;
using System.Collections.Generic;
using System.Linq;
using Until;

namespace Blog1._0.Service
{
    /// <summary>
    /// 【友链】逻辑层
    /// </summary>
    public class LinksService : Repository
    {
        /// <summary>
        /// 查询数据
        /// </summary>
        /// <returns></returns>
        public IEnumerable<LinksModel> GetAll()
        {
            var db = this.Base();
            var diay = db.IQueryable<Guo_Links>();
            var query = from q in diay
                        orderby q.CreateOn descending
                        select new LinksModel()
                        {
                            Id = q.Id,
                            Name = q.Name,
                            Url = q.Url,
                            Icon = q.Icon,
                            Describe = q.Describe,
                            CreateOn = q.CreateOn
                        };
            return query.ToList();
        }

        /// <summary>
        /// 友链 数量
        /// </summary>
        /// <returns></returns>
        public int GetCount()
        {
            return this.Base().IQueryable<Guo_Links>().Count();
        }
    }
}
