﻿using Blog1._0.Data;
using Blog1._0.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using Until;

namespace Blog1._0.Service
{
    /// <summary>
    /// 【友情链接】逻辑层
    /// </summary>
    public class Admin_LinksService : Repository
    {
        /// <summary>
        /// 数据加载
        /// </summary>
        /// <param name="filter"></param>
        /// <param name="pageInfo"></param>
        /// <returns></returns>
        public LayuiResult GetListByFilter(Guo_Links filter, PageInfo pageInfo)
        {
            var db = this.Base();
            var link = db.IQueryable<Guo_Links>();
            var query = (from i in link
                         orderby i.CreateOn descending
                         select i).Skip((pageInfo.page - 1) * pageInfo.limit).Take(pageInfo.limit);

            return new LayuiResult { code = 0, count = link.Count(), data = query.ToList() };
        }

        /// <summary>
        /// 新增
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public bool CreateModel(LinksModel model)
        {
            var db = this.Base();
            Guo_Links diay = new Guo_Links();
            db.SaveChanges();
            diay.Name = model.Name;
            diay.Url = model.Url;
            diay.Icon = model.Icon;
            diay.Describe = model.Describe;
            diay.CreateOn = model.CreateOn;

            db.Insert<Guo_Links>(diay);
            //提交失败，则检查实体类是否加上自增ID标识
            return db.Commit() > 0 ? true : false;
        }

        /// <summary>
        /// 详情
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public IEnumerable<LinksModel> ReadModel(int Id)
        {
            var db = this.Base();
            var data = db.IQueryable<Guo_Links>().Where(t => t.Id == Id);
            var query = from i in data
                        select new LinksModel()
                        {
                            Id = i.Id,
                            Name = i.Name,
                            Url = i.Url,
                            Icon = i.Icon,
                            Describe = i.Describe,
                            CreateOn = i.CreateOn
                        };

            return query.ToList();
        }

        /// <summary>
        /// 修改
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public bool UpdateList(LinksModel model)
        {
            var db = this.Base();
            var res = db.IQueryable<Guo_Links>(t => t.Id == model.Id).FirstOrDefault();
            res.Name = model.Name;
            res.Url = model.Url;
            res.Icon = model.Icon;
            res.Describe = model.Describe;

            db.Update(res);
            return db.Commit() > 0 ? true : false;
        }

        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public bool DeleteModel(int Id)
        {
            var db = this.Base();
            try
            {
                var entity = db.FindEntity<Guo_Links>(t => t.Id == Id);
                if (entity == null)
                {
                    return false;
                }
                db.Delete<Guo_Links>(t => t.Id == entity.Id);
                return db.Commit() > 0 ? true : false;
            }
            catch (Exception)
            {
                // db.Rollback();
                throw new Exception("删除失败！");
            }
        }
    }
}
