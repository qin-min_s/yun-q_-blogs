﻿using Blog1._0.Data;
using Blog1._0.Model;
using System.Collections.Generic;
using System.Linq;
using Until;

namespace Blog1._0.Service
{
    /// <summary>
    /// 【菜单管理】逻辑层
    /// </summary>
    public class Admin_MenuService : Repository
    {
        /// <summary>
        /// 查询菜单数据
        /// </summary>
        /// <returns></returns>
        public IEnumerable<MenuModel> GetAll()
        {
            var db = this.Base();
            var diay = db.IQueryable<Guo_AdminMenu>();
            var query = from q in diay
                        select new MenuModel()
                        {
                            Id = q.ID,
                            MenuName = q.MenuName,
                            MenuUrl = q.MenuUrl,
                            MenuIcon = q.MenuIcon,
                            CreateOn = q.CreateOn,
                            Status = q.Status,
                            OrderNo = q.OrderNo,
                            ParentId = q.ParentId
                        };
            return query.ToList();
        }

        /// <summary>
        /// 新增
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public int CreateModel(MenuModel model)
        {
            var db = this.Base();
            Guo_AdminMenu diay = new Guo_AdminMenu();
            diay.MenuName = model.MenuName;
            diay.MenuIcon = model.MenuIcon;
            diay.ParentId = model.ParentId;
            diay.MenuUrl = model.MenuUrl;
            diay.Status = model.Status;
            diay.OrderNo = model.OrderNo;
            diay.CreateOn = model.CreateOn;
            diay.CreateBy = model.CreateBy;

            db.Insert<Guo_AdminMenu>(diay);
            db.SaveChanges();

            //提交失败，则检查实体类是否加上自增ID标识
            return db.Commit() > 0 ? diay.ID : 0;
        }

        /// <summary>
        /// 权限新增
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public bool CreateModel_List(MenuRoleActionModel model)
        {
            var db = this.Base();
            Guo_Admin_Role_Menu role = new Guo_Admin_Role_Menu();
            role.MenuId = model.MenuId;
            role.RoleId = model.RoleId;
            role.ActionId = model.ActionId;

            db.Insert<Guo_Admin_Role_Menu>(role);
            return db.Commit() > 0 ? true : false;
        }

        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="menid"></param>
        /// <returns></returns>
        public bool DeleteMenuAllByMenuId(int menid)
        {
            var db = this.Base();
            var menu = db.FindEntity<Guo_AdminMenu>(t => t.ID == menid);
            if (menu == null)
            {
                return false;
            }

            db.Delete<Guo_AdminMenu>(t => t.ID == menu.ID);
            db.Delete<Guo_Admin_Role_Menu>(t => t.MenuId == menu.ID);
            db.Delete<Guo_Menu_Action>(t => t.MenuId == menu.ID);
            return db.Commit() > 0 ? true : false;
        }

        /// <summary>
        /// 详情
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public IEnumerable<MenuModel> ReadModel(int Id)
        {
            var db = this.Base();
            var data = db.IQueryable<Guo_AdminMenu>().Where(t => t.ID == Id);
            var query = from i in data
                        select new MenuModel()
                        {
                            Id = i.ID,
                            MenuName = i.MenuName,
                            MenuIcon = i.MenuIcon,
                            ParentId = i.ParentId,
                            MenuUrl = i.MenuUrl,
                            Status = i.Status,
                            OrderNo = i.OrderNo,
                            CreateBy = i.CreateBy,
                            CreateOn = i.CreateOn
                        };

            return query.ToList();
        }

        /// <summary>
        /// 获取上级菜单名
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public string GetParentMenuName(int Id)
        {
            var db = this.Base();
            var menu = db.FindEntity<Guo_AdminMenu>(t => t.ID == Id);
            var parent_id = menu.ParentId;
            var data = db.FindEntity<Guo_AdminMenu>(t => t.ID == parent_id);

            return data == null ? "" : data.MenuName;
        }

        /// <summary>
        /// 修改
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public bool UpdateList(MenuModel model)
        {
            var db = this.Base();
            var res = db.IQueryable<Guo_AdminMenu>(t => t.ID == model.Id).FirstOrDefault();
            res.MenuName = model.MenuName;
            res.MenuIcon = model.MenuIcon;
            res.ParentId = model.ParentId;
            res.MenuUrl = model.MenuUrl;
            res.Status = model.Status;
            res.OrderNo = model.OrderNo;
            res.UpdateOn = model.UpdateOn;
            res.UpdateBy = model.UpdateBy;

            db.Update(res);
            return db.Commit() > 0 ? true : false;
        }

        /// <summary>
        /// 查询菜单权限
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public IEnumerable<ActionModel> GetActionListByMenuId(int Id)
        {
            var db = this.Base();
            var menu_action = db.IQueryable<Guo_Menu_Action>(t => t.MenuId == Id);
            var action = db.IQueryable<Guo_Action>();
            var query = from i in menu_action
                        join t in action on
                        i.ActionId equals t.ID
                        select new ActionModel()
                        {
                            Id = t.ID,
                            ActionCode = t.ActionCode,
                            ActionName = t.ActionName,
                            Position = t.Position,
                            Icon = t.Icon,
                            Method = t.Method,
                            Remark = t.Remark,
                            OrderBy = t.OrderBy,
                            ClassName = t.ClassName
                        };
            return query;
        }

        /// <summary>
        /// 返回菜单按钮数据
        /// </summary>
        /// <returns></returns>
        public IEnumerable<ActionModel> GetAll_List()
        {
            var db = this.Base();
            var diay = db.IQueryable<Guo_Action>();
            var query = from q in diay
                        select new ActionModel()
                        {
                            ActionCode = q.ActionCode,
                            ActionName = q.ActionName,
                            Position = q.Position,
                            Icon = q.Remark,
                            Method = q.Method,
                            OrderBy = q.OrderBy,
                            ClassName = q.ClassName,
                            Id = q.ID
                        };
            return query;
        }

        /// <summary>
        /// 保存菜单权限配置
        /// </summary>
        /// <param name="list"></param>
        /// <param name="menuId"></param>
        /// <returns></returns>
        public bool GetMenuList(IEnumerable<MenuActionModel> list, int menuId)
        {
            var db = this.Base();
            //先删除，再插入
            var action = db.FindList<Guo_Menu_Action>(t => t.MenuId == menuId);
            if (action != null)
            {
                db.Delete(action);
                db.SaveChanges();

                Guo_Menu_Action menu = new Guo_Menu_Action();
                foreach (var item in list)
                {
                    menu.ActionId = item.ActionId;
                    menu.MenuId = item.MenuId;
                    db.Insert(menu);
                    db.SaveChanges();
                }

                db.Commit();
                return menu != null ? true : false;
            }
            else
            {
                Guo_Menu_Action menu = new Guo_Menu_Action();
                foreach (var item in list)
                {
                    menu.ActionId = item.ActionId;
                    menu.MenuId = item.MenuId;
                    db.Insert<Guo_Menu_Action>(menu);
                    db.SaveChanges();
                }

                db.Commit();
                return menu != null ? true : false;
            }
        }
    }
}
