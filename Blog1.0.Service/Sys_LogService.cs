﻿using Blog1._0.Data;
using Blog1._0.Model;
using System;
using Until;

namespace Blog1._0.Service
{
    public class Sys_LogService : Repository
    {
        /// <summary>
        /// 写入日志
        /// </summary>
        /// <param name="log"></param>
        public void WriteLog(string message, Category type, bool results = false)
        {
            try
            {
                var db = this.Base();
                var user = Current.GetEntity();

                Guo_SysLog log = new Guo_SysLog();
                log.Type = type.ToString();
                log.Time = DateTime.Now;
                log.OP_Eid = user.OperatorId;
                log.OP_Ename = user.UserName;
                log.Describe = message;
                log.Ip = Net.Ip;
                log.IpAddressName = Net.Host;
                log.Results = results;
                db.Insert<Guo_SysLog>(log);
                db.Commit();
            }
            catch (Exception)
            {
                throw new Exception("保存日志出现错误！");
            }
        }
    }
}
