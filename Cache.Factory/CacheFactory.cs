﻿using Cache;
namespace Cache.Factory
{
    public class CacheFactory
    {

        public static ICache Cache()
        {
            string cacheType = "Cache";
            switch (cacheType)
            {
                case "Cache":
                    return new Cache();
                case "Redis":
                    return null;
                default:
                    return new Cache();
            }
        }
    }
}
