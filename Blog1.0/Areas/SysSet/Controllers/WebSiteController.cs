﻿using Blog1._0.Service;
using System;
using System.Web.Mvc;
using Until;

namespace Blog1._0.Areas.SysSet.Controllers
{
    /// <summary>
    /// 【网站设置】控制器
    /// </summary>
    public class WebSiteController : MvcControllerBase
    {
        /// <summary>
        /// 网站设置 视图
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public ActionResult Index(int? Id)
        {
            #region 查询按钮权限
            ActionService actionService = new ActionService();
            WebSiteInfo siteInfo = new WebSiteInfo();
            var _menuId = Id == null ? 0 : Id.Value;
            var _roleId = Current.GetEntity().RoleId;
            if (Id != null)
            {
                ViewData["ActionList"] = actionService.GetActionListByMenuIdRoleId(_menuId, _roleId, PositionEnum.FormInside);
                ViewData["ActionFormRightTop"] = actionService.GetActionListByMenuIdRoleId(_menuId, _roleId, PositionEnum.FormRightTop);
            }
            #endregion

            return View(siteInfo.GetWebSiteInfo());
        }

        /// <summary>
        /// 修改
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Index(WebSiteModel model)
        {
            WebSiteInfo siteInfo = new WebSiteInfo();

            try
            {
                Configs.SetValue("MaxCommentNum", model.MaxCommentNum);
                Configs.SetValue("MaxFeedbackNum", model.MaxFeedbackNum);
                Configs.SetValue("OpenComment", model.OpenComment == "on" ? "true" : "false");
                Configs.SetValue("OpenFeedback", model.OpenFeedback == "on" ? "true" : "false");
                Configs.SetValue("SiteName", model.SiteName);
                Configs.SetValue("SiteTitle", model.SiteTitle);
                Configs.SetValue("SiteDomain", model.SiteDomain);
                Configs.SetValue("QQ", model.QQ);
                Configs.SetValue("Mail", model.Mail);
                Configs.SetValue("Address", model.Address);
                Configs.SetValue("Gitee", model.Gitee);
                Configs.SetValue("MetaKey", model.MetaKey);
                Configs.SetValue("MetaDescribe", model.MetaDescribe);
                Configs.SetValue("MaxFileUpload", model.MaxFileUpload);
                Configs.SetValue("HomeTitle", model.HomeTitle);
                Configs.SetValue("CacheTime", model.CacheTime);
                Configs.SetValue("CopyRight", model.CopyRight);
                Configs.SetValue("UploadFileType", model.UploadFileType);
                Configs.SetValue("Frontface", model.Frontface);
                Configs.SetValue("AboutFont", model.AboutFont);
            }
            catch (Exception ex)
            {
                ViewBag.Msg = "Error:" + ex.Message;
                return View(siteInfo.GetWebSiteInfo());
            }
            ViewBag.Msg = "修改成功！";
            return View(siteInfo.GetWebSiteInfo());
        }
    }
}