﻿using Blog1._0.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Until;

namespace Blog1._0.Areas.Permissions.Controllers
{
    /// <summary>
    /// 【菜单管理】控制器
    /// </summary>
    public class MenuController : MvcControllerBase
    {
        Admin_MenuService service = new Admin_MenuService();
        /// <summary>
        /// 菜单管理视图
        /// </summary>
        /// <returns></returns>
        public ActionResult Index(int? Id)
        {
            #region 查询按钮权限
            ActionService actionService = new ActionService();
            var _menuId = Id == null ? 0 : Id.Value;
            var _roleId = Current.GetEntity().RoleId;
            if (Id != null)
            {
                ViewData["ActionList"] = actionService.GetActionListByMenuIdRoleId(_menuId, _roleId, PositionEnum.FormInside);
                ViewData["ActionFormRightTop"] = actionService.GetActionListByMenuIdRoleId(_menuId, _roleId, PositionEnum.FormRightTop);
            }
            #endregion
            return View();
        }

        /// <summary>
        /// 数据加载
        /// </summary>
        /// <param name="pageInfo"></param>
        /// <param name="filter"></param>
        /// <returns></returns>
        [HttpGet]
        public JsonResult List(PageInfo pageInfo, MenuModel filter)
        {
            var list = service.GetAll();
            var result = new { code = 0, count = list.Count(), data = list };
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 添加菜单视图
        /// </summary>
        /// <returns></returns>
        public ActionResult Add()
        {
            return View();
        }

        /// <summary>
        /// 添加菜单方法
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Add(MenuModel model)
        {
            model.CreateOn = DateTime.Now;
            model.CreateBy = Current.GetEntity().OperatorId;
            int MenId = service.CreateModel(model); //获取新增的菜单ID

            if (MenId != 0)
            {
                //在Guo_Admin_Role_Menu新增一行(菜单ID,0,0)记录,即新增的菜单没有角色和权限按钮
                MenuRoleActionModel menuRoleActionModel = new MenuRoleActionModel()
                {
                    MenuId = MenId,
                    RoleId = 0,
                    ActionId = 0
                };
                service.CreateModel_List(menuRoleActionModel);

                //在Guo_Admin_Role_Menu新增一行(菜单ID,1,0)记录,即新增的菜单超级管理员有菜单权限但没按钮权限
                MenuRoleActionModel menuRoleActionTwoModel = new MenuRoleActionModel()
                {
                    MenuId = MenId,
                    RoleId = 1,
                    ActionId = 0
                };
                service.CreateModel_List(menuRoleActionTwoModel);

            }
            var result = MenId > 0 ? SuccessTip() : ErrorTip();
            return Json(result);
        }

        /// <summary>
        /// 删除菜单
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Delete(int Id)
        {
            //删除菜单时,同时删除菜单权限,菜单角色权限记录
            var result = service.DeleteMenuAllByMenuId(Id) ? SuccessTip() : ErrorTip();
            return Json(result);
        }

        /// <summary>
        /// 获取上级菜单
        /// </summary>
        /// <param name="isIndex"></param>
        /// <returns></returns>
        public JsonResult GetMenuList(bool isIndex = false)
        {
            AdminService admin = new AdminService();
            object result = admin.GetMenusList(isIndex, Current.GetEntity().RoleId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 编辑
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public ActionResult Edit(int Id)
        {
            var model = service.ReadModel(Id).FirstOrDefault();
            ViewBag.ParentMenuName = service.GetParentMenuName(Id);//获取上级菜单
            return View(model);
        }

        /// <summary>
        /// 修改方法
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Edit(MenuModel model)
        {
            model.UpdateOn = DateTime.Now;
            model.UpdateBy = Current.GetEntity().OperatorId;
            var result = service.UpdateList(model) ? SuccessTip() : ErrorTip();
            return Json(result);
        }

        /// <summary>
        /// 菜单权限
        /// </summary>
        /// <param name="pageInfo"></param>
        /// <param name="filter"></param>
        /// <param name="roleId"></param>
        /// <returns></returns>         
        public ActionResult MenuActionList(int Id)
        {
            var model = service.ReadModel(Id).FirstOrDefault();
            ViewBag.Id = model.Id;
            ViewBag.ParentId = model.ParentId;
            ViewBag.MenuName = model.MenuName;
            ViewData["AvailableMenuActionList"] = service.GetActionListByMenuId(Id).ToList();
            ViewData["MenuActionList"] = service.GetAll_List().ToList();
            return View();
        }

        /// <summary>
        /// 保存菜单权限配置
        /// </summary>
        /// <param name="list"></param>
        /// <param name="menuId"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult InsertBatch(IEnumerable<MenuActionModel> list, int menuId)
        {
            var result = service.GetMenuList(list, menuId) ? SuccessTip() : ErrorTip();
            return Json(result);
        }


    }
}