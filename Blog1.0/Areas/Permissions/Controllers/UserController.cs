﻿using Blog1._0.Service;
using System;
using System.Linq;
using System.Web.Mvc;
using Until;

namespace Blog1._0.Areas.Permissions.Controllers
{
    /// <summary>
    /// 【用户管理】控制器
    /// </summary>
    public class UserController : MvcControllerBase
    {
        Admin_UserService service = new Admin_UserService();
        /// <summary>
        /// 用户管理 首页视图
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public ActionResult Index(int? Id)
        {
            #region 查询按钮权限
            ActionService actionService = new ActionService();
            var _menuId = Id == null ? 0 : Id.Value;
            var _roleId = Current.GetEntity().RoleId;
            if (Id != null)
            {
                ViewData["ActionList"] = actionService.GetActionListByMenuIdRoleId(_menuId, _roleId, PositionEnum.FormInside);
                ViewData["ActionFormRightTop"] = actionService.GetActionListByMenuIdRoleId(_menuId, _roleId, PositionEnum.FormRightTop);
            }
            #endregion

            ViewBag.RoleId = RoleList;
            return View();
        }

        /// <summary>
        /// 新增视图
        /// </summary>
        /// <returns></returns>
        public ActionResult Add()
        {
            ViewBag.RoleId = RoleList;
            return View();
        }

        /// <summary>
        /// 详情视图
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public ActionResult Detail(int Id)
        {
            var model = service.ReadModel(Id).FirstOrDefault();
            return View(model);
        }

        /// <summary>
        /// 编辑视图
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public ActionResult Edit(int Id)
        {
            var model = service.ReadModel(Id).FirstOrDefault();
            ViewBag.RoleId = RoleList;
            return View(model);
        }

        /// <summary>
        /// 新增 方法
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Add(UserModel model)
        {
            model.CreateOn = DateTime.Now;
            model.CreateBy = Current.GetEntity().OperatorId;
            model.PassWord = Md5s.md5(Configs.GetValue("InitUserPwd"), 32);//获取初始密码并加密
            var result = service.CreateModel(model) ? SuccessTip() : ErrorTip();
            return Json(result);
        }

        /// <summary>
        /// 角色名称选择下拉列表
        /// </summary>
        public SelectList RoleList
        {
            get
            {
                return new SelectList(service.GetRoleList(), "Id", "RoleName");
            }
        }

        /// <summary>
        /// 加载数据列表
        /// </summary>
        /// <param name="filter"></param>
        /// <param name="pageInfo"></param>
        /// <returns></returns>
        [HttpGet]
        public JsonResult List(UserModel filter, PageInfo pageInfo)
        {
            var result = service.GetListByFilter(filter, pageInfo);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 保存修改
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Edit(UserModel model)
        {
            model.UpdateOn = DateTime.Now;
            model.UpdateBy = Current.GetEntity().OperatorId;
            var result = service.UpdateList(model) ? SuccessTip() : ErrorTip();
            return Json(result);
        }

        /// <summary>
        /// 删除用户
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Delete(int Id)
        {
            var result = service.DeleteModel(Id) ? SuccessTip() : ErrorTip();
            return Json(result);
        }

        /// <summary>
        /// 重置密码
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult InitPwd(int Id)
        {
            var initPwd = Md5s.md5(Configs.GetValue("InitUserPwd"), 32);
            UserModel model = new UserModel { Id = Id, PassWord = initPwd };
            var result = service.InitPwd(model) ? SuccessTip() : ErrorTip();
            return Json(result);
        }
    }
}