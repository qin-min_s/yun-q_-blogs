﻿using System.Web.Mvc;
using Until;

namespace Blog1._0.Controllers
{
    /// <summary>
    /// 【关于】控制器
    /// </summary>
    public class AboutController : Controller
    {
        /// <summary>
        /// 首页视图
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            WebSiteInfo siteInfo = new WebSiteInfo();
            ViewBag.SiteTitle = siteInfo.GetWebSiteInfo().SiteTitle;
            ViewBag.AboutFont = siteInfo.GetWebSiteInfo().AboutFont;
            return View();
        }
    }
}