﻿using Blog1._0.Service;
using System.Web.Mvc;
using Until;

namespace Blog1._0
{
    //[SessionState(SessionStateBehavior.ReadOnly)]  //同一个session并发时为异步
    [LoginAuthorize(LoginMode.Enforce)]
    public abstract class MvcControllerBase : Controller
    {        
        /// <summary>
        /// 返回成功消息
        /// </summary>
        /// <param name="data">数据</param>
        /// <returns></returns>
        protected virtual ActionResult ToJsonResult(object data)
        {
            return Content(data.ToJson());
        }
        /// <summary>
        /// 返回成功消息
        /// </summary>
        /// <param name="message">消息</param>
        /// <returns></returns>
        protected virtual ActionResult Success(string message = "操作成功！")
        {
            var res = new Result { type = true, msg = message };
            return Content(res.ToJson());
        }
        /// <summary>
        /// 返回成功消息
        /// </summary>
        /// <param name="message">消息</param>
        /// <param name="data">数据</param>
        /// <returns></returns>
        protected virtual ActionResult Success(object data, string message = "操作成功！")
        {
            return Content((new Result { type = true, msg = message, data = data }).ToJson());
        }
        /// <summary>
        /// 返回失败消息
        /// </summary>
        /// <param name="data">数据</param>
        /// <returns></returns>
        protected virtual ActionResult Error(object data)
        {
            return Content(data.ToJson());
        }
        /// <summary>
        /// 返回失败消息
        /// </summary>
        /// <param name="message">消息</param>
        /// <returns></returns>
        protected virtual ActionResult Error(string message = "操作失败！")
        {
            return Content((new Result { type = false, msg = message }).ToJson());
        }
        /// <summary>
        /// 返回失败消息
        /// </summary>
        /// <param name="message">消息</param>
        /// <param name="data">数据</param>
        /// <returns></returns>
        protected virtual ActionResult Error(object data, string message = "操作失败！")
        {
            return Content((new Result { type = false, msg = message, data = data }).ToJson());
        }

        /// <summary>
        /// 写入日志
        /// </summary>
        public void WriteLog(Result res, Category type)
        {
            Sys_LogService logService = new Sys_LogService();
            logService.WriteLog(res.msg, type, res.type);
        }

        /// <summary>
        /// 按钮 权限
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        //public virtual ActionResult Index(int? id)
        //{
        //    ActionService actionService = new ActionService();
        //    var _menuId = id == null ? 0 : id.Value;
        //    var _roleId = Current.GetEntity().RoleId;
        //    if (id != null)
        //    {
        //        ViewData["ActionList"] = actionService.GetActionListByMenuIdRoleId(_menuId, _roleId, PositionEnum.FormInside);
        //        ViewData["ActionFormRightTop"] = actionService.GetActionListByMenuIdRoleId(_menuId, _roleId, PositionEnum.FormRightTop);
        //    }
        //    return View();
        //}

        /// <summary>
        /// 操作成功
        /// </summary>
        /// <param name="message">提示文本</param>
        /// <returns></returns>
        protected virtual AjaxResult SuccessTip(string message = "操作成功！")
        {
            return new AjaxResult { state = ResultType.success.ToString(), message = message };
        }
        /// <summary>
        /// 操作失败
        /// </summary>
        /// <param name="message">提示文本</param>
        /// <returns></returns>
        protected virtual AjaxResult ErrorTip(string message = "操作失败！")
        {
            return new AjaxResult { state = ResultType.error.ToString(), message = message };
        }
    }
}
